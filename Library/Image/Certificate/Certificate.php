<?php namespace Library\Image\Certificate;

use Library\Image\RepoAbstractImage;
use PHPImageWorkshop\ImageWorkshop;


class Certificate extends RepoAbstractImage implements CertificateInterface{

    protected $background;
    protected $backgroundEnvelop;

    /**
     *
     */
    public function __construct()
    {
        $this->background = ImageWorkshop::initFromPath(__DIR__ . '/../../../img/background.png');
        $this->backgroundEnvelop = ImageWorkshop::initFromPath(__DIR__ . '/../../../img/background_1.png');
        parent::__construct();
    }

    /**
     * @param $name
     * @param $imageUrl
     * @param $saveAttribute
     * @return mixed|string
     */
    public function getCertificate($name , $imageUrl , $saveAttribute)
    {
        $imageLayer = $this->getProfileImage($imageUrl);
        $imageLayer->resizeInPixel(250, null, true);
        $this->background->addLayerOnTop($imageLayer,0,0,'MM');
        
        $this->background->addLayerOnTop($this->backgroundEnvelop, 0 ,0, 'MM');

        $textLayer = $this->getNameImage($name);
        $this->background->addLayerOnTop($textLayer,0,260,'MT');

        $finalImage = $this->background->getResult('fef5d8');

        if(!is_null($saveAttribute))
        {
            $dirPath         = $_SERVER['DOCUMENT_ROOT']."/image/storage/certificate/".date('Y');
            $filename        = time().str_replace(' ','',$name).".jpg";
            $createFolders   = true;
            $backgroundColor = 'fef5d8';
            $imageQuality    = 95;

            $this->background->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);
        }
        return $finalImage;

    }

} 
