<?php

namespace Library\Image\Certificate;


interface CertificateInterface {

    /**
     * @param $name
     * @param $imageUrl
     * @param $saveAttribute
     * @return mixed
     */
    public function getCertificate($name , $imageUrl, $saveAttribute);
} 