<?php namespace Library\Image\Goodwill;

use Library\Image\RepoAbstractImage;
use PHPImageWorkshop\ImageWorkshop;

class Goodwill extends RepoAbstractImage implements GoodwillInterface
{


    protected $background;
    protected $backgroundEnvelop;

    /**
     *
     */
    public function __construct()
    {
        $this->background = ImageWorkshop::initFromPath(__DIR__ . '/../../../img/goodwill_transparent.png');
        $this->backgroundEnvelop = ImageWorkshop::initFromPath(__DIR__ . '/../../../img/background_goodwill.png');
        parent::__construct();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getGoodwill($data, $saveAttribute)
    {

        $imageLayer_1 = $this->getProfileImage($data[1]);
        $imageLayer_1->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_1, 100, 55, 'MT');

        $imageLayer_2 = $this->getProfileImage($data[2]);
        $imageLayer_2->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_2, -100, 55, 'MT');

        $imageLayer_3 = $this->getProfileImage($data[3]);
        $imageLayer_3->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_3, 265, 175, 'MT');

        $imageLayer_4 = $this->getProfileImage($data[4]);
        $imageLayer_4->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_4, -265, 175, 'MT');

        $imageLayer_5 = $this->getProfileImage($data[5]);
        $imageLayer_5->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_5, 330, 375, 'MT');

        $imageLayer_6 = $this->getProfileImage($data[6]);
        $imageLayer_6->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_6, -330, 375, 'MT');

        $imageLayer_7 = $this->getProfileImage($data[7]);
        $imageLayer_7->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_7, 265, 135, 'MM');

        $imageLayer_8 = $this->getProfileImage($data[8]);
        $imageLayer_8->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_8, -265, 135, 'MM');

        $imageLayer_9 = $this->getProfileImage($data[9]);
        $imageLayer_9->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_9, 100, 245, 'MM');

        $imageLayer_10 = $this->getProfileImage($data[10]);
        $imageLayer_10->resizeInPixel(140, null, true);
        $this->background->addLayerOnTop($imageLayer_10, -100, 245, 'MM');

        $this->background->addLayerOnTop($this->backgroundEnvelop, 0, 0, 'MM');
        $finalImage = $this->background->getResult('fef5d8');


        if (!is_null($saveAttribute)) {
            $dirPath = $_SERVER['DOCUMENT_ROOT'] . "/image/storage/goodwill/" . date('Y');
            $filename = time() . ".jpg";
            $createFolders = true;
            $backgroundColor = 'fef5d8';
            $imageQuality = 95;

            $this->background->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);
        }
        return $finalImage;
    }
}
