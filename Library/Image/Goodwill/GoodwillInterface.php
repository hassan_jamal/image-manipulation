<?php


namespace Library\Image\Goodwill;


interface GoodwillInterface
{

    /**
     * @param $data
     * @return mixed
     */
    public function getGoodwill($data, $saveAttribute);
}