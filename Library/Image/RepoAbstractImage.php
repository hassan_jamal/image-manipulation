<?php namespace Library\Image;

use PHPImageWorkshop\ImageWorkshop;

abstract class RepoAbstractImage {

    protected $api_secret;
    protected $api_key;
    protected $attribute;

    /**
     *
     */
    protected function __construct()
    {
        $this->api_secret         = 'rpO6OciZ6oDfLz8f8_4Jb54XbIQGYuyh';
        $this->api_key            = '276526051fc2b3f720ba898e0619251d';
        $this->attribute          = 'glass,pose,gender,age,race,smiling';
    }

    /**
     * @param $url
     * @return \PHPImageWorkshop\Core\ImageWorkshopLayer
     * @throws \PHPImageWorkshop\Exception\ImageWorkshopException
     */
    protected function getProfileImage($url)
    {
        
        $imageDir    = $_SERVER['DOCUMENT_ROOT'] . "/image/storage/temp";
        $imageFile   = $imageDir . '/picture_'.time().'.jpg';
        $defaultFile = __DIR__ . '/../../img/default.png';

        if (file_exists($imageFile)) {
            unlink($imageFile);
        }
        $ch = curl_init($url);
        $fp = fopen($imageFile, 'w');

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        $imageAttribute = $this->getImageAttribute($imageFile);

        if ($imageAttribute && $imageAttribute->face) {
            $imageLayer = ImageWorkshop::initFromPath($imageFile);
            return $imageLayer;
        } else {
            $imageLayer = ImageWorkshop::initFromPath($defaultFile);
            return $imageLayer;
        }
    }

    /**
     * @param $imagePath
     * @return mixed
     */
    protected function getImageAttribute($imagePath)
    {

        // $data = [
        //     'img'        => new \CURLFile($imagePath),
        //     'api_secret' => $this->api_secret,
        //     'api_key'    => $this->api_key,
        //     'attribute'  => $this->attribute
        // ];

        /* In case CURLFile doesn't work
            comment above array and 
            uncomment below array */

       $data = [
           'img'        => '@' . $imagePath,
           'api_secret' => $this->api_secret,
           'api_key'    => $this->api_key,
           'attribute'  => $this->attribute
       ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://apius.faceplusplus.com/v2/detection/detect');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);

        return json_decode($response);
    }

    /**
     * @param $name
     * @return \PHPImageWorkshop\Core\ImageWorkshopLayer
     */
    protected function getNameImage($name)
    {
        $text         = $name;
        $fontPath     = __DIR__."/../../Font/courier_new.ttf";
        $fontSize     = 25;
        $fontColor    = "000000";
        $textRotation = 0;
        $textLayer    = ImageWorkshop::initTextLayer($text, $fontPath, $fontSize, $fontColor, $textRotation);

        return $textLayer;
    }

}
