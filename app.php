<?php

ini_set('display_errors',1);
error_reporting(E_ALL);

require 'vendor/autoload.php';

//enable these 3 to see whole JSON object returned from face++
ini_set('xdebug.var_display_max_depth', 50);
ini_set('xdebug.var_display_max_children', 256);
ini_set('xdebug.var_display_max_data', 10024);

if( isset($_GET['name']) )
{

    /****** CERTIFICATE *******/

    // $name          = "Hassan Jamal";
    // $imageUrl      = 'https://scontent-a.xx.fbcdn.net/hphotos-xfa1/v/l/t1.0-9/10151885_684337088290874_1161737516_n.jpg?oh=8e5e364f7d9fe75381b2c58ed42e7b4c&oe=54839967';

    $name          = $_GET['name'];
    $imageUrl      = $_GET['url'];

    /*  set attribute to null if you don't want to store
        image will be store in
        document_root/storage/certificate/".date('Y') */
    $saveAttribute    = true;


    $certificate      = new \Library\Image\Certificate\Certificate();
    $imageCertificate = $certificate->getCertificate($name,$imageUrl, $saveAttribute);

    /* comment bellow if you dont want to show in browser */

    header('Content-type: image/jpeg');
    header('Content-Disposition: filename="butterfly.jpg"');
    imagejpeg($imageCertificate, null, 95);
    exit;

}
else
{

    /****** GOODWILL *******/

    // $data = [];
    // for ($i=1; $i <= 10 ; $i++) {
    //     $data[$i]['name'] = 'Demo Name' ;
    //     $data[$i]['url']  = 'http://lorempixel.com/600/600/people/';
    // }

    $data = $_GET['img'];
    /*  set attribute to null if you don't want to store
        image will be store in
        document_root/storage/certificate/".date('Y') */

    $saveAttribute = true;
    $goodwill      = new \Library\Image\Goodwill\Goodwill();
    $imageGoodwill = $goodwill->getGoodwill($data , $saveAttribute);


    /* comment bellow if you dont want to show in browser */

    header('Content-type: image/jpeg');
    header('Content-Disposition: filename="butterfly.jpg"');
    imagejpeg($imageGoodwill, null, 95);
    exit;

}
